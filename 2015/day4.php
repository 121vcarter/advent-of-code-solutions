<?php

/**
 * https://adventofcode.com/2015/day/4
 */

$input = 'bgvyzdsv';

$answer1 = null;
$answer2 = null;

$counter = 0;
$searchPattern = '/^00000/';

while(true) {
    $hash = md5($input . ++$counter);
    $isValidHash = !!preg_match($searchPattern, $hash);
    
    if (!$isValidHash) {
        continue;
    }

    if (!$answer1) {
        $answer1 = $counter;
        $searchPattern = '/^000000/';
        continue;
    }

    $answer2 = $counter;
    break;
}

var_dump($answer1, $answer2);
